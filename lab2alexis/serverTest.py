#reminder: this is a comment. the first line imports a default "socket" into Python.
#you dont have to install this. the second line is initialization to add TCP/IP protocal to the endpoint.
import socket
serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#assigns a port for the server that listens to clients connecting to this port.
serv.bind(('0.0.0.0', 8080))
serv.listen(5)
while True:
    conn, addr = serv.accept()
    from_client = ''
    while True:
        data = conn.recv(4096)
        if not data: break
        from_client += data
        print(from_client)
        conn.send("I am SERVER\n")
    conn.close()
    print('client disconnected')
    
