"use strict"

const e = React.createElement

function getParameterByName(name, url) {
  if (!url) url = window.location.href
  name = name.replace(/[\[\]]/g, "\\$&")
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url)
  if (!results) return null
  if (!results[2]) return ""
  return decodeURIComponent(results[2].replace(/\+/g, " "))
}

class LikeButton extends React.Component {
  constructor(props) {
    super(props)
    this.state = { liked: false, value: 1, occupied: "False" }
  }

  componentDidMount() {
    var seat = getParameterByName("seat")
    const rootRef = firebase.database().ref()
    const valueRef = rootRef
      .child("seat")
      .child(seat)
      .child("value")
    valueRef.on("value", (snap) => {
      this.setState({
        value: snap.val()
      })
    })
    const occupiedRef = rootRef
      .child("seat")
      .child(seat)
      .child("occupied")
    valueRef.on("occupied", (snap) => {
      this.setState({
        occupied: snap.val()
      })
    })
  }

  render() {
    if (this.state.value == 1) {
      return e(
        "div",
        {
          style: { width: "100vw", height: "100vh", backgroundColor: "white" }
        },
        null
      )
    } else if (this.state.value == 0) {
      return e(
        "div",
        {
          style: { width: "100vw", height: "100vh", backgroundColor: "black" }
        },
        null
      )
    }
  }
}

const domContainer = document.querySelector("#like_button_container")
ReactDOM.render(e(LikeButton), domContainer)
