"use strict"

class ReactSeat extends React.Component {
  constructor(props) {
    super(props)
    this.state = { liked: false }
  }

  render() {
    return e(
      "div",
      { class: "col-xs-2" },
      e(
        "button",
        {
          id: this.props.id,
          class: "btn btn-default btn-block center-block seat-button"
        },
        this.props.id
      )
    )
  }
}

const e = React.createElement

class SeatsRoot extends React.Component {
  constructor(props) {
    super(props)
    this.state = { liked: false }
  }

  render() {
    return e(
      "div",
      null,
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "1Asel" }),
        e(ReactSeat, { id: "1Bsel" }),
        e(ReactSeat, { id: "1Csel" }),
        e(ReactSeat, { id: "1Dsel" }),
        e(ReactSeat, { id: "1Esel" }),
        e(ReactSeat, { id: "1Fsel" })
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "2Asel" }),
        e(ReactSeat, { id: "2Bsel" }),
        e(ReactSeat, { id: "2Csel" }),
        e(ReactSeat, { id: "2Dsel" }),
        e(ReactSeat, { id: "2Esel" }),
        e(ReactSeat, { id: "2Fsel" })
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "3Asel" }),
        e(ReactSeat, { id: "3Bsel" }),
        e(ReactSeat, { id: "3Csel" }),
        e(ReactSeat, { id: "3Dsel" }),
        e(ReactSeat, { id: "3Esel" }),
        e(ReactSeat, { id: "3Fsel" })
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "4Asel" }),
        e(ReactSeat, { id: "4Bsel" }),
        e(ReactSeat, { id: "4Csel" }),
        e(ReactSeat, { id: "4Dsel" }),
        e(ReactSeat, { id: "4Esel" }),
        e(ReactSeat, { id: "4Fsel" })
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "5Asel" }),
        e(ReactSeat, { id: "5Bsel" }),
        e(ReactSeat, { id: "5Csel" }),
        e(ReactSeat, { id: "5Dsel" }),
        e(ReactSeat, { id: "5Esel" }),
        e(ReactSeat, { id: "5Fsel" })
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "6Asel" }),
        e(ReactSeat, { id: "6Bsel" }),
        e(ReactSeat, { id: "6Csel" }),
        e(ReactSeat, { id: "6Dsel" }),
        e(ReactSeat, { id: "6Esel" }),
        e(ReactSeat, { id: "6Fsel" })
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "7Asel" }),
        e(ReactSeat, { id: "7Bsel" }),
        e(ReactSeat, { id: "7Csel" }),
        e(ReactSeat, { id: "7Dsel" }),
        e(ReactSeat, { id: "7Esel" }),
        e(ReactSeat, { id: "7Fsel" })
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "8Asel" }),
        e(ReactSeat, { id: "8Bsel" }),
        e(ReactSeat, { id: "8Csel" }),
        e(ReactSeat, { id: "8Dsel" }),
        e(ReactSeat, { id: "8Esel" }),
        e(ReactSeat, { id: "8Fsel" })
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "9Asel" }),
        e(ReactSeat, { id: "9Bsel" }),
        e(ReactSeat, { id: "9Csel" }),
        e(ReactSeat, { id: "9Dsel" }),
        e(ReactSeat, { id: "9Esel" }),
        e(ReactSeat, { id: "9Fsel" })
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "10Asel" }),
        e(ReactSeat, { id: "10Bsel" }),
        e(ReactSeat, { id: "10Csel" }),
        e(ReactSeat, { id: "10Dsel" }),
        e(ReactSeat, { id: "10Esel" }),
        e(ReactSeat, { id: "10Fsel" })
      )
    )
  }
}

const domContainer = document.querySelector("#root")
ReactDOM.render(e(SeatsRoot), domContainer)
