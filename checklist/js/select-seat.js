"use strict"

class ReactSeat extends React.Component {
  constructor(props) {
    super(props)
    this.state = { liked: false, occupied: "False" }
  }

  componentDidMount() {
    //var seat = getParameterByName("seat")
    const rootRef = firebase.database().ref()
    const valueRef = rootRef
      .child("seat")
      .child(this.props.id)
      .child("occupied")
    valueRef.on("value", (snap) => {
      this.setState({
        occupied: snap.val()
      })
    })
  }

  render() {
    console.log("occupied: %b", this.state.occupied)
    if (this.state.occupied == "True") {
      return e(
        "div",
        { class: "col-xs-2" },
        e(
          "button",
          {
            id: this.props.id,
            class: "btn btn-default btn-block center-block seat-button",
            disabled: !this.props.check,
            style: { backgroundColor: "#fd7e14", color: "white" }
          },
          this.props.id
        )
      )
    } else {
      return e(
        "div",
        { class: "col-xs-2" },
        e(
          "button",
          {
            id: this.props.id,
            class: "btn btn-default btn-block center-block seat-button",
            disabled: !this.props.check,
            style: { backgroundColor: "#007bff", color: "white" }
          },
          this.props.id
        )
      )
    }
  }
}

const e = React.createElement

class SeatsRoot extends React.Component {
  constructor(props) {
    super(props)
    this.state = { checkone: false, checktwo: false, checkthree: false }
    this.handleCheck1 = this.handleCheck1.bind(this)
    this.handleCheck2 = this.handleCheck2.bind(this)
    this.handleCheck3 = this.handleCheck3.bind(this)
  }

  handleCheck1 = () => {
    this.setState({ checkone: !this.state.checkone })
  }
  handleCheck2 = () => {
    this.setState({ checktwo: !this.state.checktwo })
  }
  handleCheck3 = () => {
    this.setState({ checkthree: !this.state.checkthree })
  }
  render() {
    var check = false
    if (this.state.checkone && this.state.checktwo && this.state.checkthree) {
      check = true
    } else check = false

    return e(
      "div",
      null,
      e(
        "div",
        { class: "row mb4 mt40" },
        e(
          "h2",
          { style: { marginLeft: "5px" } },
          "Please ensure you have done the following before selecting a seat"
        )
      ),
      e(
        "div",
        { class: "row mb4", style: { display: "flex" } },
        e("input", {
          type: "checkbox",
          defaultChecked: false,
          checked: this.state.checkone,
          onChange: this.handleCheck1
        }),
        e(
          "p",
          { style: { marginLeft: "5px" } },
          "I have turned my brightness up."
        )
      ),
      e(
        "div",
        { class: "row mb4", style: { display: "flex" } },
        e("input", {
          type: "checkbox",
          defaultChecked: false,
          checked: this.state.checktwo,
          onChange: this.handleCheck2
        }),
        e(
          "p",
          { style: { marginLeft: "5px" } },
          "I have turned off Auto-Dimming."
        )
      ),
      e(
        "div",
        { class: "row mb40", style: { display: "flex" } },
        e("input", {
          type: "checkbox",
          defaultChecked: false,
          checked: this.state.checkthree,
          onChange: this.handleCheck3
        }),
        e("p", { style: { marginLeft: "5px" } }, "I have turned off Auto-Lock.")
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "1A", check: check }),
        e(ReactSeat, { id: "1B", check: check }),
        e(ReactSeat, { id: "1C", check: check }),
        e(ReactSeat, { id: "1D", check: check }),
        e(ReactSeat, { id: "1E", check: check }),
        e(ReactSeat, { id: "1F", check: check })
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "2A", check: check }),
        e(ReactSeat, { id: "2B", check: check }),
        e(ReactSeat, { id: "2C", check: check }),
        e(ReactSeat, { id: "2D", check: check }),
        e(ReactSeat, { id: "2E", check: check }),
        e(ReactSeat, { id: "2F", check: check })
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "3A", check: check }),
        e(ReactSeat, { id: "3B", check: check }),
        e(ReactSeat, { id: "3C", check: check }),
        e(ReactSeat, { id: "3D", check: check }),
        e(ReactSeat, { id: "3E", check: check }),
        e(ReactSeat, { id: "3F", check: check })
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "4A", check: check }),
        e(ReactSeat, { id: "4B", check: check }),
        e(ReactSeat, { id: "4C", check: check }),
        e(ReactSeat, { id: "4D", check: check }),
        e(ReactSeat, { id: "4E", check: check }),
        e(ReactSeat, { id: "4F", check: check })
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "5A", check: check }),
        e(ReactSeat, { id: "5B", check: check }),
        e(ReactSeat, { id: "5C", check: check }),
        e(ReactSeat, { id: "5D", check: check }),
        e(ReactSeat, { id: "5E", check: check }),
        e(ReactSeat, { id: "5F", check: check })
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "6A", check: check }),
        e(ReactSeat, { id: "6B", check: check }),
        e(ReactSeat, { id: "6C", check: check }),
        e(ReactSeat, { id: "6D", check: check }),
        e(ReactSeat, { id: "6E", check: check }),
        e(ReactSeat, { id: "6F", check: check })
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "7A", check: check }),
        e(ReactSeat, { id: "7B", check: check }),
        e(ReactSeat, { id: "7C", check: check }),
        e(ReactSeat, { id: "7D", check: check }),
        e(ReactSeat, { id: "7E", check: check }),
        e(ReactSeat, { id: "7F", check: check })
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "8A", check: check }),
        e(ReactSeat, { id: "8B", check: check }),
        e(ReactSeat, { id: "8C", check: check }),
        e(ReactSeat, { id: "8D", check: check }),
        e(ReactSeat, { id: "8E", check: check }),
        e(ReactSeat, { id: "8F", check: check })
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "9A", check: check }),
        e(ReactSeat, { id: "9B", check: check }),
        e(ReactSeat, { id: "9C", check: check }),
        e(ReactSeat, { id: "9D", check: check }),
        e(ReactSeat, { id: "9E", check: check }),
        e(ReactSeat, { id: "9F", check: check })
      ),
      e(
        "div",
        { class: "row mb40" },
        e(ReactSeat, { id: "10A", check: check }),
        e(ReactSeat, { id: "10B", check: check }),
        e(ReactSeat, { id: "10C", check: check }),
        e(ReactSeat, { id: "10D", check: check }),
        e(ReactSeat, { id: "10E", check: check }),
        e(ReactSeat, { id: "10F", check: check })
      )
    )
  }
}

const domContainer = document.querySelector("#root")
ReactDOM.render(e(SeatsRoot), domContainer)
