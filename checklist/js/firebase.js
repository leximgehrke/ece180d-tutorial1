
var firebaseConfig = {
	apiKey: "AIzaSyBNg7L7RJFT_cOBfZRlxVZ9PEsEBlYIIc8",
	authDomain: "gradcapapp.firebaseapp.com",
	databaseURL: "https://gradcapapp.firebaseio.com",
	projectId: "gradcapapp",
	storageBucket: "gradcapapp.appspot.com",
	messagingSenderId: "53673671013",
	appId: "1:53673671013:web:5e789b76f12e9c1cc7498c",
	measurementId: "G-0Z2L4M7R72"
};

firebase.initializeApp(firebaseConfig);

var myFirebase = firebase.database().ref();
var gradcapapp = myFirebase.child("seat");

var submitSeat = function (s) {
	//Get input values from each of the form elements
	var seat = s;
  var updates = {};
  updates['/seat/' + seat + '/occupied'] = "True";

  firebase.database().ref().update(updates);
};

var resetSeat = function (s) {
  //Get input values from each of the form elements
  var seat = s;
  // Push a new recommendation to the database using those values
  firebase.database().ref('seat/' + seat).set({
    "occupied": "False", 
    "value": "0"
  });
};


var updateVal = function (seat, value) {
  firebase.database().ref('seat/' + seat).set({
    "value" : value
      });
};


$(document).ready(function(){
  $("#Send").click(function () {
    submitSeat("Send");
    $("#selection").slideUp();
    $("#Form").slideUp();
    document.getElementById("caption").innerHTML = "Your message was successfully submitted";
  });
});


$(document).ready(function(){
  $("#Reset").click(function () {
    resetSeat("1A");
    resetSeat("1B");
    resetSeat("1C");
    resetSeat("1D");
    resetSeat("1E");
    resetSeat("1F");

    resetSeat("2A");
    resetSeat("2B");
    resetSeat("2C");
    resetSeat("2D");
    resetSeat("2E");
    resetSeat("2F");

    resetSeat("3A");
    resetSeat("3B");
    resetSeat("3C");
    resetSeat("3D");
    resetSeat("3E");
    resetSeat("3F");

    resetSeat("4A");
    resetSeat("4B");
    resetSeat("4C");
    resetSeat("4D");
    resetSeat("4E");
    resetSeat("4F");

    resetSeat("5A");
    resetSeat("5B");
    resetSeat("5C");
    resetSeat("5D");
    resetSeat("5E");
    resetSeat("5F");

    resetSeat("6A");
    resetSeat("6B");
    resetSeat("6C");
    resetSeat("6D");
    resetSeat("6E");
    resetSeat("6F");

    resetSeat("7A");
    resetSeat("7B");
    resetSeat("7C");
    resetSeat("7D");
    resetSeat("7E");
    resetSeat("7F");

    resetSeat("8A");
    resetSeat("8B");
    resetSeat("8C");
    resetSeat("8D");
    resetSeat("8E");
    resetSeat("8F");

    resetSeat("9A");
    resetSeat("9B");
    resetSeat("9C");
    resetSeat("9D");
    resetSeat("9E");
    resetSeat("9F");

    resetSeat("10A");
    resetSeat("10B");
    resetSeat("10C");
    resetSeat("10D");
    resetSeat("10E");
    resetSeat("10F");

    $("#selection").slideUp();
    $("#Form").slideUp();
  });
});

$(document).ready(function(){
  $("#1A").click(function () {
  	submitSeat("1A");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry0").innerHTML = "Continue to Display";
  });
});

$(document).ready(function(){
  $("#1B").click(function () {
  	submitSeat("1B");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry1").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#1C").click(function () {
  	submitSeat("1C");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry2").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#1D").click(function () {
  	submitSeat("1D");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry3").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#1E").click(function () {
  	submitSeat("1E");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry4").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#1F").click(function () {
  	submitSeat("1F");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry5").innerHTML = "Continue to Display";
  });
});


$(document).ready(function(){
  $("#2A").click(function () {
  	submitSeat("2A");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry6").innerHTML = "Continue to Display";
  });
});

$(document).ready(function(){
  $("#2B").click(function () {
  	submitSeat("2B");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry7").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#2C").click(function () {
  	submitSeat("2C");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry8").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#2D").click(function () {
  	submitSeat("2D");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry9").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#2E").click(function () {
  	submitSeat("2E");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry10").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#2F").click(function () {
  	submitSeat("2F");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry11").innerHTML = "Continue to Display";
  });
});


$(document).ready(function(){
  $("#3A").click(function () {
  	submitSeat("3A");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry12").innerHTML = "Continue to Display";
  });
});

$(document).ready(function(){
  $("#3B").click(function () {
  	submitSeat("3B");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry13").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#3C").click(function () {
  	submitSeat("3C");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry14").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#3D").click(function () {
  	submitSeat("3D");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry15").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#3E").click(function () {
  	submitSeat("3E");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry16").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#3F").click(function () {
  	submitSeat("3F");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry17").innerHTML = "Continue to Display";
  });
});


$(document).ready(function(){
  $("#4A").click(function () {
  	submitSeat("4A");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry18").innerHTML = "Continue to Display";
  });
});

$(document).ready(function(){
  $("#4B").click(function () {
  	submitSeat("4B");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry19").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#4C").click(function () {
  	submitSeat("4C");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry20").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#4D").click(function () {
  	submitSeat("4D");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry21").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#4E").click(function () {
  	submitSeat("4E");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry22").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#4F").click(function () {
  	submitSeat("4F");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry23").innerHTML = "Continue to Display";
  });
});



$(document).ready(function(){
  $("#5A").click(function () {
  	submitSeat("5A");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry24").innerHTML = "Continue to Display";
  });
});

$(document).ready(function(){
  $("#5B").click(function () {
  	submitSeat("5B");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry25").innerHTML = "Continue to Display";  });
});
$(document).ready(function(){
  $("#5C").click(function () {
  	submitSeat("5C");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry26").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#5D").click(function () {
  	submitSeat("5D");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry27").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#5E").click(function () {
  	submitSeat("5E");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry28").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#5F").click(function () {
  	submitSeat("5F");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry29").innerHTML = "Continue to Display";
  });
});



$(document).ready(function(){
  $("#6A").click(function () {
  	submitSeat("6A");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry30").innerHTML = "Continue to Display";
  });
});

$(document).ready(function(){
  $("#6B").click(function () {
  	submitSeat("6B");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry31").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#6C").click(function () {
  	submitSeat("6C");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry32").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#6D").click(function () {
  	submitSeat("6D");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry33").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#6E").click(function () {
  	submitSeat("6E");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry34").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#6F").click(function () {
  	submitSeat("6F");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry35").innerHTML = "Continue to Display";
  });
});



$(document).ready(function(){
  $("#7A").click(function () {
  	submitSeat("7A");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry36").innerHTML = "Continue to Display";
  });
});

$(document).ready(function(){
  $("#7B").click(function () {
  	submitSeat("7B");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry37").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#7C").click(function () {
  	submitSeat("7C");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry38").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#7D").click(function () {
  	submitSeat("7D");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry39").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#7E").click(function () {
  	submitSeat("7E");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry40").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#7F").click(function () {
  	submitSeat("7F");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry41").innerHTML = "Continue to Display";
  });
});



$(document).ready(function(){
  $("#8A").click(function () {
  	submitSeat("8A");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry42").innerHTML = "Continue to Display";
  });
});

$(document).ready(function(){
  $("#8B").click(function () {
  	submitSeat("8B");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry43").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#8C").click(function () {
  	submitSeat("8C");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry44").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#8D").click(function () {
  	submitSeat("8D");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry45").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#8E").click(function () {
  	submitSeat("8E");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry46").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#8F").click(function () {
  	submitSeat("8F");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry47").innerHTML = "Continue to Display";
  });
});



$(document).ready(function(){
  $("#9A").click(function () {
  	submitSeat("9A");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry48").innerHTML = "Continue to Display";
  });
});

$(document).ready(function(){
  $("#9B").click(function () {
  	submitSeat("9B");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry49").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#9C").click(function () {
  	submitSeat("9C");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry50").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#9D").click(function () {
  	submitSeat("9D");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry51").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#9E").click(function () {
  	submitSeat("9E");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry52").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#9F").click(function () {
  	submitSeat("9F");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry53").innerHTML = "Continue to Display";
  });
});



$(document).ready(function(){
  $("#10A").click(function () {
  	submitSeat("10A");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
   	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry54").innerHTML = "Continue to Display";
  });
});

$(document).ready(function(){
  $("#10B").click(function () {
  	submitSeat("10B");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry55").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#10C").click(function () {
  	submitSeat("10C");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry56").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#10D").click(function () {
  	submitSeat("10D");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry57").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#10E").click(function () {
  	submitSeat("10E");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry58").innerHTML = "Continue to Display";
  });
});
$(document).ready(function(){
  $("#10F").click(function () {
  	submitSeat("10F");
  	$("#selection").slideUp();
  	$("#Form").slideUp();
  	document.getElementById("caption").innerHTML = "Your seat was successfully submitted";
  	document.getElementById("newentry59").innerHTML = "Continue to Display";
  });
});

var setColor = function (btn) {
  var ref = document.getElementById(btn)
  if (ref.style.color == "green")
    ref.style.color = "blue";
  else
    ref.style.color = "green";
};

var setValue = function () {
  //Get input values from each of the form elements
  var updates = {};

  if (document.getElementById("1Asel").style.color == "green")
    updates['/seat/1A/value'] = "1";
  else
    updates['/seat/1A/value'] = "0";

  if (document.getElementById("1Bsel").style.color == "green")
    updates['/seat/1B/value'] = "1";
  else
    updates['/seat/1B/value'] = "0";

  if (document.getElementById("1Csel").style.color == "green")
    updates['/seat/1C/value'] = "1";
  else
    updates['/seat/1C/value'] = "0";

  if (document.getElementById("1Dsel").style.color == "green")
    updates['/seat/1D/value'] = "1";
  else
    updates['/seat/1D/value'] = "0";

  if (document.getElementById("1Esel").style.color == "green")
    updates['/seat/1E/value'] = "1";
  else
    updates['/seat/1E/value'] = "0";

  if (document.getElementById("1Fsel").style.color == "green")
    updates['/seat/1F/value'] = "1";
  else
    updates['/seat/1F/value'] = "0";

  if (document.getElementById("2Asel").style.color == "green")
    updates['/seat/2A/value'] = "1";
  else
    updates['/seat/2A/value'] = "0";

  if (document.getElementById("2Bsel").style.color == "green")
    updates['/seat/2B/value'] = "1";
  else
    updates['/seat/2B/value'] = "0";

  if (document.getElementById("2Csel").style.color == "green")
    updates['/seat/2C/value'] = "1";
  else
    updates['/seat/2C/value'] = "0";

  if (document.getElementById("2Dsel").style.color == "green")
    updates['/seat/2D/value'] = "1";
  else
    updates['/seat/2D/value'] = "0";

  if (document.getElementById("2Esel").style.color == "green")
    updates['/seat/2E/value'] = "1";
  else
    updates['/seat/2E/value'] = "0";

  if (document.getElementById("2Fsel").style.color == "green")
    updates['/seat/2F/value'] = "1";
  else
    updates['/seat/2F/value'] = "0";

  if (document.getElementById("3Asel").style.color == "green")
    updates['/seat/3A/value'] = "1";
  else
    updates['/seat/3A/value'] = "0";

  if (document.getElementById("3Bsel").style.color == "green")
    updates['/seat/3B/value'] = "1";
  else
    updates['/seat/3B/value'] = "0";

  if (document.getElementById("3Csel").style.color == "green")
    updates['/seat/3C/value'] = "1";
  else
    updates['/seat/3C/value'] = "0";

  if (document.getElementById("3Dsel").style.color == "green")
    updates['/seat/3D/value'] = "1";
  else
    updates['/seat/3D/value'] = "0";

  if (document.getElementById("3Esel").style.color == "green")
    updates['/seat/3E/value'] = "1";
  else
    updates['/seat/3E/value'] = "0";

  if (document.getElementById("3Fsel").style.color == "green")
    updates['/seat/3F/value'] = "1";
  else
    updates['/seat/3F/value'] = "0";

  if (document.getElementById("4Asel").style.color == "green")
    updates['/seat/4A/value'] = "1";
  else
    updates['/seat/4A/value'] = "0";

  if (document.getElementById("4Bsel").style.color == "green")
    updates['/seat/4B/value'] = "1";
  else
    updates['/seat/4B/value'] = "0";

  if (document.getElementById("4Csel").style.color == "green")
    updates['/seat/4C/value'] = "1";
  else
    updates['/seat/4C/value'] = "0";

  if (document.getElementById("4Dsel").style.color == "green")
    updates['/seat/4D/value'] = "1";
  else
    updates['/seat/4D/value'] = "0";

  if (document.getElementById("4Esel").style.color == "green")
    updates['/seat/4E/value'] = "1";
  else
    updates['/seat/4E/value'] = "0";

  if (document.getElementById("4Fsel").style.color == "green")
    updates['/seat/4F/value'] = "1";
  else
    updates['/seat/4F/value'] = "0";

  if (document.getElementById("5Asel").style.color == "green")
    updates['/seat/5A/value'] = "1";
  else
    updates['/seat/5A/value'] = "0";

  if (document.getElementById("5Bsel").style.color == "green")
    updates['/seat/5B/value'] = "1";
  else
    updates['/seat/5B/value'] = "0";

  if (document.getElementById("5Csel").style.color == "green")
    updates['/seat/5C/value'] = "1";
  else
    updates['/seat/5C/value'] = "0";

  if (document.getElementById("5Dsel").style.color == "green")
    updates['/seat/5D/value'] = "1";
  else
    updates['/seat/5D/value'] = "0";

  if (document.getElementById("5Esel").style.color == "green")
    updates['/seat/5E/value'] = "1";
  else
    updates['/seat/5E/value'] = "0";

  if (document.getElementById("5Fsel").style.color == "green")
    updates['/seat/5F/value'] = "1";
  else
    updates['/seat/5F/value'] = "0";

  if (document.getElementById("6Asel").style.color == "green")
    updates['/seat/6A/value'] = "1";
  else
    updates['/seat/6A/value'] = "0";

  if (document.getElementById("6Bsel").style.color == "green")
    updates['/seat/6B/value'] = "1";
  else
    updates['/seat/6B/value'] = "0";

  if (document.getElementById("6Csel").style.color == "green")
    updates['/seat/6C/value'] = "1";
  else
    updates['/seat/6C/value'] = "0";

  if (document.getElementById("6Dsel").style.color == "green")
    updates['/seat/6D/value'] = "1";
  else
    updates['/seat/6D/value'] = "0";

  if (document.getElementById("6Esel").style.color == "green")
    updates['/seat/6E/value'] = "1";
  else
    updates['/seat/6E/value'] = "0";

  if (document.getElementById("6Fsel").style.color == "green")
    updates['/seat/6F/value'] = "1";
  else
    updates['/seat/6F/value'] = "0";

  if (document.getElementById("7Asel").style.color == "green")
    updates['/seat/7A/value'] = "1";
  else
    updates['/seat/7A/value'] = "0";

  if (document.getElementById("7Bsel").style.color == "green")
    updates['/seat/7B/value'] = "1";
  else
    updates['/seat/7B/value'] = "0";

  if (document.getElementById("7Csel").style.color == "green")
    updates['/seat/7C/value'] = "1";
  else
    updates['/seat/7C/value'] = "0";

  if (document.getElementById("7Dsel").style.color == "green")
    updates['/seat/7D/value'] = "1";
  else
    updates['/seat/7D/value'] = "0";

  if (document.getElementById("7Esel").style.color == "green")
    updates['/seat/7E/value'] = "1";
  else
    updates['/seat/7E/value'] = "0";

  if (document.getElementById("7Fsel").style.color == "green")
    updates['/seat/7F/value'] = "1";
  else
    updates['/seat/7F/value'] = "0";

  if (document.getElementById("8Asel").style.color == "green")
    updates['/seat/8A/value'] = "1";
  else
    updates['/seat/8A/value'] = "0";

  if (document.getElementById("8Bsel").style.color == "green")
    updates['/seat/8B/value'] = "1";
  else
    updates['/seat/8B/value'] = "0";

  if (document.getElementById("8Csel").style.color == "green")
    updates['/seat/8C/value'] = "1";
  else
    updates['/seat/8C/value'] = "0";

  if (document.getElementById("8Dsel").style.color == "green")
    updates['/seat/8D/value'] = "1";
  else
    updates['/seat/8D/value'] = "0";

  if (document.getElementById("8Esel").style.color == "green")
    updates['/seat/8E/value'] = "1";
  else
    updates['/seat/8E/value'] = "0";

  if (document.getElementById("8Fsel").style.color == "green")
    updates['/seat/8F/value'] = "1";
  else
    updates['/seat/8F/value'] = "0";

  if (document.getElementById("9Asel").style.color == "green")
    updates['/seat/9A/value'] = "1";
  else
    updates['/seat/9A/value'] = "0";

  if (document.getElementById("9Bsel").style.color == "green")
    updates['/seat/9B/value'] = "1";
  else
    updates['/seat/9B/value'] = "0";

  if (document.getElementById("9Csel").style.color == "green")
    updates['/seat/9C/value'] = "1";
  else
    updates['/seat/9C/value'] = "0";

  if (document.getElementById("9Dsel").style.color == "green")
    updates['/seat/9D/value'] = "1";
  else
    updates['/seat/9d/value'] = "0";

  if (document.getElementById("9Esel").style.color == "green")
    updates['/seat/9E/value'] = "1";
  else
    updates['/seat/9E/value'] = "0";

  if (document.getElementById("9Fsel").style.color == "green")
    updates['/seat/9F/value'] = "1";
  else
    updates['/seat/9F/value'] = "0";

  if (document.getElementById("10Asel").style.color == "green")
    updates['/seat/10A/value'] = "1";
  else
    updates['/seat/10A/value'] = "0";

  if (document.getElementById("10Bsel").style.color == "green")
    updates['/seat/10B/value'] = "1";
  else
    updates['/seat/10B/value'] = "0";

  if (document.getElementById("10Csel").style.color == "green")
    updates['/seat/10C/value'] = "1";
  else
    updates['/seat/10C/value'] = "0";

  if (document.getElementById("10Dsel").style.color == "green")
    updates['/seat/10D/value'] = "1";
  else
    updates['/seat/10D/value'] = "0";

  if (document.getElementById("10Esel").style.color == "green")
    updates['/seat/10E/value'] = "1";
  else
    updates['/seat/10E/value'] = "0";

  if (document.getElementById("10Fsel").style.color == "green")
    updates['/seat/10F/value'] = "1";
  else
    updates['/seat/10F/value'] = "0";

  firebase.database().ref().update(updates);
};

$(document).ready(function(){
  $("#stuff").click(function () {
      $("#selection").slideUp();
      $("#Form").slideUp();
      setValue();
  });
});


$(document).ready(function(){
  $("#1Asel").click(function () {
      setColor("1Asel");
  });
});
$(document).ready(function(){
  $("#1Bsel").click(function () {
      setColor("1Bsel");
  });
});
$(document).ready(function(){
  $("#1Csel").click(function () {
      setColor("1Csel");
  });
});
$(document).ready(function(){
  $("#1Dsel").click(function () {
      setColor("1Dsel");
  });
});
$(document).ready(function(){
  $("#1Esel").click(function () {
      setColor("1Esel");
  });
});
$(document).ready(function(){
  $("#1Fsel").click(function () {
      setColor("1Fsel");
  });
});

$(document).ready(function(){
  $("#2Asel").click(function () {
      setColor("2Asel");
  });
});
$(document).ready(function(){
  $("#2Bsel").click(function () {
      setColor("2Bsel");
  });
});
$(document).ready(function(){
  $("#2Csel").click(function () {
      setColor("2Csel");
  });
});
$(document).ready(function(){
  $("#2Dsel").click(function () {
      setColor("2Dsel");
  });
});
$(document).ready(function(){
  $("#2Esel").click(function () {
      setColor("2Esel");
  });
});
$(document).ready(function(){
  $("#2Fsel").click(function () {
      setColor("2Fsel");
  });
});

$(document).ready(function(){
  $("#3Asel").click(function () {
      setColor("3Asel");
  });
});
$(document).ready(function(){
  $("#3Bsel").click(function () {
      setColor("3Bsel");
  });
});
$(document).ready(function(){
  $("#3Csel").click(function () {
      setColor("3Csel");
  });
});
$(document).ready(function(){
  $("#3Dsel").click(function () {
      setColor("3Dsel");
  });
});
$(document).ready(function(){
  $("#3Esel").click(function () {
      setColor("3Esel");
  });
});
$(document).ready(function(){
  $("#3Fsel").click(function () {
      setColor("3Fsel");
  });
});

$(document).ready(function(){
  $("#4Asel").click(function () {
      setColor("4Asel");
  });
});
$(document).ready(function(){
  $("#4Bsel").click(function () {
      setColor("4Bsel");
  });
});
$(document).ready(function(){
  $("#4Csel").click(function () {
      setColor("4Csel");
  });
});
$(document).ready(function(){
  $("#4Dsel").click(function () {
      setColor("4Dsel");
  });
});
$(document).ready(function(){
  $("#4Esel").click(function () {
      setColor("4Esel");
  });
});
$(document).ready(function(){
  $("#4Fsel").click(function () {
      setColor("4Fsel");
  });
});

$(document).ready(function(){
  $("#5Asel").click(function () {
      setColor("5Asel");
  });
});
$(document).ready(function(){
  $("#5Bsel").click(function () {
      setColor("5Bsel");
  });
});
$(document).ready(function(){
  $("#5Csel").click(function () {
      setColor("5Csel");
  });
});
$(document).ready(function(){
  $("#5Dsel").click(function () {
      setColor("5Dsel");
  });
});
$(document).ready(function(){
  $("#5Esel").click(function () {
      setColor("5Esel");
  });
});
$(document).ready(function(){
  $("#5Fsel").click(function () {
      setColor("5Fsel");
  });
});

$(document).ready(function(){
  $("#6Asel").click(function () {
      setColor("6Asel");
  });
});
$(document).ready(function(){
  $("#6Bsel").click(function () {
      setColor("6Bsel");
  });
});
$(document).ready(function(){
  $("#6Csel").click(function () {
      setColor("6Csel");
  });
});
$(document).ready(function(){
  $("#6Dsel").click(function () {
      setColor("6Dsel");
  });
});
$(document).ready(function(){
  $("#6Esel").click(function () {
      setColor("6Esel");
  });
});
$(document).ready(function(){
  $("#6Fsel").click(function () {
      setColor("6Fsel");
  });
});

$(document).ready(function(){
  $("#7Asel").click(function () {
      setColor("7Asel");
  });
});
$(document).ready(function(){
  $("#7Bsel").click(function () {
      setColor("7Bsel");
  });
});
$(document).ready(function(){
  $("#7Csel").click(function () {
      setColor("7Csel");
  });
});
$(document).ready(function(){
  $("#7Dsel").click(function () {
      setColor("7Dsel");
  });
});
$(document).ready(function(){
  $("#7Esel").click(function () {
      setColor("7Esel");
  });
});
$(document).ready(function(){
  $("#7Fsel").click(function () {
      setColor("7Fsel");
  });
});

$(document).ready(function(){
  $("#8Asel").click(function () {
      setColor("8Asel");
  });
});
$(document).ready(function(){
  $("#8Bsel").click(function () {
      setColor("8Bsel");
  });
});
$(document).ready(function(){
  $("#8Csel").click(function () {
      setColor("8Csel");
  });
});
$(document).ready(function(){
  $("#8Dsel").click(function () {
      setColor("8Dsel");
  });
});
$(document).ready(function(){
  $("#8Esel").click(function () {
      setColor("8Esel");
  });
});
$(document).ready(function(){
  $("#8Fsel").click(function () {
      setColor("8Fsel");
  });
});

$(document).ready(function(){
  $("#9Asel").click(function () {
      setColor("9Asel");
  });
});
$(document).ready(function(){
  $("#9Bsel").click(function () {
      setColor("9Bsel");
  });
});
$(document).ready(function(){
  $("#9Csel").click(function () {
      setColor("9Csel");
  });
});
$(document).ready(function(){
  $("#9Dsel").click(function () {
      setColor("9Dsel");
  });
});
$(document).ready(function(){
  $("#9Esel").click(function () {
      setColor("9Esel");
  });
});
$(document).ready(function(){
  $("#9Fsel").click(function () {
      setColor("9Fsel");
  });
});

$(document).ready(function(){
  $("#10Asel").click(function () {
      setColor("10Asel");
  });
});
$(document).ready(function(){
  $("#10Bsel").click(function () {
      setColor("10Bsel");
  });
});
$(document).ready(function(){
  $("#10Csel").click(function () {
      setColor("10Csel");
  });
});
$(document).ready(function(){
  $("#10Dsel").click(function () {
      setColor("10Dsel");
  });
});
$(document).ready(function(){
  $("#10Esel").click(function () {
      setColor("10Esel");
  });
});
$(document).ready(function(){
  $("#10Fsel").click(function () {
      setColor("10Fsel");
  });
});



function doSetTimeout(k,allseats,km) {
  var p = 500*(km+1-k);
  setTimeout(function() {

      for (var i=0; i < allseats.length; i++)
      {
        for(var j=0;j<10;j++){
          var t = Math.floor((j+1)/10);
          if(t>0){
            var seat = String.fromCharCode(48+t,49+j-10*t,65+i);
          }
          else{
            var seat = String.fromCharCode(49+j,65+i);
          }
          updateVal(seat, allseats[i][j+k]);
        }
      }   
  
  }, p);
}

var A = [
  [0,0,0,0,0],
  [0,1,1,1,1],
  [0,1,0,0,1],
  [0,1,1,1,1],
  [0,1,0,0,1],
  [0,1,0,0,1],
  ];

var B = [
  [0,0,0,0,0],
  [0,1,1,1,1],
  [0,1,0,0,1],
  [0,0,1,1,1],
  [0,1,0,0,1],
  [0,1,1,1,1],
  ];

var C = [
  [0,0,0,0],
  [0,1,1,1],
  [0,0,0,1],
  [0,0,0,1],
  [0,0,0,1],
  [0,1,1,1],
  ];

var D = [
  [0,0,0,0,0],
  [0,0,1,1,1],
  [0,1,0,0,1],
  [0,1,0,0,1],
  [0,1,0,0,1],
  [0,0,1,1,1],
  ];

var E = [
  [0,0,0,0],
  [0,1,1,1],
  [0,0,0,1],
  [0,1,1,1],
  [0,0,0,1],
  [0,1,1,1],
  ];

var F = [
  [0,0,0,0],
  [0,1,1,1],
  [0,0,0,1],
  [0,1,1,1],
  [0,0,0,1],
  [0,0,0,1],
  ];

var G = [
  [0,0,0,0,0],
  [0,1,1,1,1],
  [0,0,0,0,1],
  [0,1,1,0,1],
  [0,1,0,0,1],
  [0,1,1,1,1],
  ];

var H = [
  [0,0,0,0,0],
  [0,1,0,0,1],
  [0,1,0,0,1],
  [0,1,1,1,1],
  [0,1,0,0,1],
  [0,1,0,0,1],
  ];

var I = [
  [0,0,0,0],
  [0,1,1,1],
  [0,0,1,0],
  [0,0,1,0],
  [0,0,1,0],
  [0,1,1,1],
  ];

var J = [
  [0,0,0,0],
  [0,1,1,1],
  [0,0,1,0],
  [0,0,1,0],
  [0,0,1,0],
  [0,0,1,1],
  ];

var K = [
  [0,0,0,0,0],
  [0,1,0,0,1],
  [0,0,1,0,1],
  [0,0,0,1,1],
  [0,0,1,0,1],
  [0,1,0,0,1],
  ];

var L = [
  [0,0,0,0],
  [0,0,0,1],
  [0,0,0,1],
  [0,0,0,1],
  [0,0,0,1],
  [0,1,1,1],
  ];

var M = [
  [0,0,0,0,0,0],
  [0,1,0,0,0,1],
  [0,1,1,0,1,1],
  [0,1,0,1,0,1],
  [0,1,0,0,0,1],
  [0,1,0,0,0,1],
  ];

var N = [
  [0,0,0,0,0,0],
  [0,1,0,0,0,1],
  [0,1,0,0,1,1],
  [0,1,0,1,0,1],
  [0,1,1,0,0,1],
  [0,1,0,0,0,1],
  ];

var O = [
  [0,0,0,0,0],
  [0,1,1,1,1],
  [0,1,0,0,1],
  [0,1,0,0,1],
  [0,1,0,0,1],
  [0,1,1,1,1],
  ];

var P = [
  [0,0,0,0],
  [0,1,1,1],
  [0,1,0,1],
  [0,1,1,1],
  [0,0,0,1],
  [0,0,0,1],
  ];

var Q = [
  [0,0,0,0,0,0],
  [0,0,1,1,1,1],
  [0,0,1,0,0,1],
  [0,0,1,1,0,1],
  [0,0,1,1,1,1],
  [0,1,0,0,0,0],
  ];

var R = [
  [0,0,0,0],
  [0,0,1,1],
  [0,1,0,1],
  [0,0,1,1],
  [0,1,0,1],
  [0,1,0,1],
  ];

var S = [
  [0,0,0,0],
  [0,1,1,1],
  [0,0,0,1],
  [0,1,1,1],
  [0,1,0,0],
  [0,1,1,1],
  ];

var T = [
  [0,0,0,0],
  [0,1,1,1],
  [0,0,1,0],
  [0,0,1,0],
  [0,0,1,0],
  [0,0,1,0],
  ];

var U = [
  [0,0,0,0],
  [0,1,0,1],
  [0,1,0,1],
  [0,1,0,1],
  [0,1,0,1],
  [0,1,1,1],
  ];

var V = [
  [0,0,0,0],
  [0,1,0,1],
  [0,1,0,1],
  [0,1,0,1],
  [0,1,0,1],
  [0,0,1,0],
  ];

var W = [
  [0,0,0,0,0,0],
  [0,1,0,1,0,1],
  [0,1,0,1,0,1],
  [0,1,0,1,0,1],
  [0,1,0,1,0,1],
  [0,0,1,0,1,0],
  ];

var X = [
  [0,0,0,0,0,0],
  [0,1,0,0,0,1],
  [0,0,1,0,1,0],
  [0,0,0,1,0,0],
  [0,0,1,0,1,0],
  [0,1,0,0,0,1],
  ];

var Y = [
  [0,0,0,0],
  [0,1,0,1],
  [0,1,0,1],
  [0,0,1,0],
  [0,0,1,0],
  [0,0,1,0],
  ];

var Z = [
  [0,0,0,0],
  [0,1,1,1],
  [0,1,0,0],
  [0,0,1,0],
  [0,0,0,1],
  [0,1,1,1],
  ];

var sp = [
  [0,0],
  [0,0],
  [0,0],
  [0,0],
  [0,0],
  [0,0],
  ];

var ex = [
  [0,0],
  [0,1],
  [0,1],
  [0,1],
  [0,0],
  [0,1],
  ];

var apos = [
  [0,1],
  [0,1],
  [0,0],
  [0,0],
  [0,0],
  [0,0],
  ];

var per = [
  [0,0],
  [0,0],
  [0,0],
  [0,0],
  [0,0],
  [0,1],
  ];

var ques = [
  [0,1,1,1],
  [0,1,0,0],
  [0,1,1,0],
  [0,0,1,0],
  [0,0,0,0],
  [0,0,1,0],
  ];

var bor = [
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  ];

var displayCustom = function(s)
{

  var allseats = [
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  ];

  for(var u = s.length-1; u>=0;u--){
    var letter = s[u];
    if(letter == ' ')
      letter = sp;
    else if(letter == '!')
      letter=ex;
    else if(letter == '\'')
      letter=apos;
    else if(letter == '.')
      letter=per;
    else if(letter == '?')
      letter=ques;
    else
      letter=window[letter];
    for(var r = 0; r<allseats.length;r++){
      allseats[r]=allseats[r].concat(letter[r]);
    }
  }

  for(var r = 0; r<allseats.length;r++){
    allseats[r]=allseats[r].concat(bor[r]);
  }

  var k = allseats[0].length-10;
  var km = k;
  while(k >= 0){
    doSetTimeout(k,allseats,km);
    k=k-1;
  }

};

$(document).ready(function(){
  $("#Custom").click(function () {
    var message = document.getElementById("mess").value.toUpperCase();
    displayCustom(message);
    var disp = "Displaying '".concat(message,"'");
    document.getElementById("caption").innerHTML = disp;
  });
});

var displayUC = function()
{
  displayCustom("Our Product is superior because it can do this!".toUpperCase());
};

$(document).ready(function(){
  $("#UC").click(function () {
    displayUC();
    document.getElementById("caption").innerHTML = "Displaying Pitch";
  });
});


var displayPres = function()
{

  var allseats = [
  [1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0],
  [0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0],
  [0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0],
  [0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1],
  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
  ];

  for(var r = 0; r<allseats.length;r++){
    allseats[r]=allseats[r].concat(allseats[r]);
  }
  var k = allseats[0].length-10;
  var km = k;
  while(k >= 0){
    doSetTimeout(k,allseats,km);
    k=k-1;
  }

};

$(document).ready(function(){
  $("#Pres").click(function () {
    displayPres();
    var disp = "Displaying Test Sequence";
    document.getElementById("caption").innerHTML = disp;
  });
});


var displayUCLA = function()
{
  displayCustom("GO UCLA!");
};

$(document).ready(function(){
  $("#UCLA").click(function () {
    displayUCLA();
    document.getElementById("caption").innerHTML = "Displaying 'GO UCLA!'";
  });
});

var displayRocks = function()
{
  displayCustom("ECE ROCKS!");
};

$(document).ready(function(){
  $("#Rocks").click(function () {
    displayRocks();
    document.getElementById("caption").innerHTML = "Displaying 'ECE ROCKS!'";
  });
});




function checkform()
    {
        var f = document.forms["theform"].elements;
        var cansubmit = true;

        for (var i = 0; i < f.length; i++) {
            if (f[i].checked == false) 
              cansubmit = false;
        }
        if (cansubmit == true){
          document.getElementById("1A").disabled = false;
          document.getElementById("1B").disabled = false;
          document.getElementById("1C").disabled = false;
          document.getElementById("1D").disabled = false;
          document.getElementById("1E").disabled = false;
          document.getElementById("1F").disabled = false;

          document.getElementById("2A").disabled = false;
          document.getElementById("2B").disabled = false;
          document.getElementById("2C").disabled = false;
          document.getElementById("2D").disabled = false;
          document.getElementById("2E").disabled = false;
          document.getElementById("2F").disabled = false;

          document.getElementById("3A").disabled = false;
          document.getElementById("3B").disabled = false;
          document.getElementById("3C").disabled = false;
          document.getElementById("3D").disabled = false;
          document.getElementById("3E").disabled = false;
          document.getElementById("3F").disabled = false;

          document.getElementById("4A").disabled = false;
          document.getElementById("4B").disabled = false;
          document.getElementById("4C").disabled = false;
          document.getElementById("4D").disabled = false;
          document.getElementById("4E").disabled = false;
          document.getElementById("4F").disabled = false;

          document.getElementById("5A").disabled = false;
          document.getElementById("5B").disabled = false;
          document.getElementById("5C").disabled = false;
          document.getElementById("5D").disabled = false;
          document.getElementById("5E").disabled = false;
          document.getElementById("5F").disabled = false;

          document.getElementById("6A").disabled = false;
          document.getElementById("6B").disabled = false;
          document.getElementById("6C").disabled = false;
          document.getElementById("6D").disabled = false;
          document.getElementById("6E").disabled = false;
          document.getElementById("6F").disabled = false;

          document.getElementById("7A").disabled = false;
          document.getElementById("7B").disabled = false;
          document.getElementById("7C").disabled = false;
          document.getElementById("7D").disabled = false;
          document.getElementById("7E").disabled = false;
          document.getElementById("7F").disabled = false;

          document.getElementById("8A").disabled = false;
          document.getElementById("8B").disabled = false;
          document.getElementById("8C").disabled = false;
          document.getElementById("8D").disabled = false;
          document.getElementById("8E").disabled = false;
          document.getElementById("8F").disabled = false;

          document.getElementById("9A").disabled = false;
          document.getElementById("9B").disabled = false;
          document.getElementById("9C").disabled = false;
          document.getElementById("9D").disabled = false;
          document.getElementById("9E").disabled = false;
          document.getElementById("9F").disabled = false;

          document.getElementById("10A").disabled = false;
          document.getElementById("10B").disabled = false;
          document.getElementById("10C").disabled = false;
          document.getElementById("10D").disabled = false;
          document.getElementById("10E").disabled = false;
          document.getElementById("10F").disabled = false;
        }
        else{
          document.getElementById("1A").disabled = true;
          document.getElementById("1B").disabled = true;
          document.getElementById("1C").disabled = true;
          document.getElementById("1D").disabled = true;
          document.getElementById("1E").disabled = true;
          document.getElementById("1F").disabled = true;

          document.getElementById("2A").disabled = true;
          document.getElementById("2B").disabled = true;
          document.getElementById("2C").disabled = true;
          document.getElementById("2D").disabled = true;
          document.getElementById("2E").disabled = true;
          document.getElementById("2F").disabled = true;

          document.getElementById("3A").disabled = true;
          document.getElementById("3B").disabled = true;
          document.getElementById("3C").disabled = true;
          document.getElementById("3D").disabled = true;
          document.getElementById("3E").disabled = true;
          document.getElementById("3F").disabled = true;

          document.getElementById("4A").disabled = true;
          document.getElementById("4B").disabled = true;
          document.getElementById("4C").disabled = true;
          document.getElementById("4D").disabled = true;
          document.getElementById("4E").disabled = true;
          document.getElementById("4F").disabled = true;

          document.getElementById("5A").disabled = true;
          document.getElementById("5B").disabled = true;
          document.getElementById("5C").disabled = true;
          document.getElementById("5D").disabled = true;
          document.getElementById("5E").disabled = true;
          document.getElementById("5F").disabled = true;

          document.getElementById("6A").disabled = true;
          document.getElementById("6B").disabled = true;
          document.getElementById("6C").disabled = true;
          document.getElementById("6D").disabled = true;
          document.getElementById("6E").disabled = true;
          document.getElementById("6F").disabled = true;

          document.getElementById("7A").disabled = true;
          document.getElementById("7B").disabled = true;
          document.getElementById("7C").disabled = true;
          document.getElementById("7D").disabled = true;
          document.getElementById("7E").disabled = true;
          document.getElementById("7F").disabled = true;

          document.getElementById("8A").disabled = true;
          document.getElementById("8B").disabled = true;
          document.getElementById("8C").disabled = true;
          document.getElementById("8D").disabled = true;
          document.getElementById("8E").disabled = true;
          document.getElementById("8F").disabled = true;

          document.getElementById("9A").disabled = true;
          document.getElementById("9B").disabled = true;
          document.getElementById("9C").disabled = true;
          document.getElementById("9D").disabled = true;
          document.getElementById("9E").disabled = true;
          document.getElementById("9F").disabled = true;

          document.getElementById("10A").disabled = true;
          document.getElementById("10B").disabled = true;
          document.getElementById("10C").disabled = true;
          document.getElementById("10D").disabled = true;
          document.getElementById("10E").disabled = true;
          document.getElementById("10F").disabled = true;
        }
    }
