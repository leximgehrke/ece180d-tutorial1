import socket

serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#assign a port for the server
serv.bind(('131.179.8.59', 8080))
serv.listen(5)
while True:
    conn, addr = serv.accept()
    from_client = ''
    while True:
        data = conn.recv(4096)
        if not data: break
        from_client += data.decode()
        print(from_client)
        conn.send("I am SERVER\n".encode())
    conn.close()
    print('client disconnected')
